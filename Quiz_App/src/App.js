import Header from './components/Header/Header'
import './App.css';
import { useState,useEffect } from "react"
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Home from './Pages/Home/Home';
import Quiz from './Pages/Quiz/Quiz';
import Result from './Pages/Result/Result';
import axios from "axios";

function App() {
  const[name,setName] = useState(""); 
  const[questions,setQuestions] = useState(''); 
  const[score,setScore] = useState(0); 
  const [errorcount, setErrorCount] = useState(0);

  useEffect(() => {
    axios({
      "method": "GET",
      "url": "https://glacial-ridge-45733.herokuapp.com/allQuestions",
    })
    .then((response) => {
      setQuestions(response.data)
      console.log("Method Calling")
    })
    .catch((error) => {
      console.log(error)
    })
  }, [])

  return (
    <BrowserRouter>
    <div className="app"style={{backgroundImage:"url(./quiz1.jpg)"}} >
     <Header/>
     <Switch>
       <Route path='/' exact>
         <Home name={name} 
         setName={setName} 
        />
       </Route>
       <Route path='/quiz' exact>
         <Quiz 
         name={name}
         questions={questions}
         score={score}
         setScore={setScore}
         errorcount={errorcount} 
         setErrorCount={setErrorCount}
         setQuestions={setQuestions}/>
       </Route>
       <Route path='/result' exact>
         <Result  
          errorcount={errorcount} 
          score={score}
          setScore={setScore}
          setName={setName} 
          name={name}/>
       </Route>
     </Switch>
    </div>
    </BrowserRouter>
  );
}

export default App;
