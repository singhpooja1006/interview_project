import {TextField} from "@material-ui/core"
import { useState,useEffect,useCallback } from "react"
import ErrorMessage from '../../components/ErrorMessage/ErrorMessage';
import { useHistory } from "react-router";
import axios from "axios";
import './Home.css'
const Home = ({name,setName  }) =>{
    const[error,setError] = useState(false); 
    const history = useHistory();
    const[finalscore,setFinalScore] = useState({}); 
    

    const handleSubmit=()=>{
        if(!name){
            setError(true);
            return;
        }
        else{
            setError(false);
           history.push("/quiz");

        }
    };
    const fetchData = useCallback(() => {
        axios({
          "method": "GET",
          "url": "https://glacial-ridge-45733.herokuapp.com/getFinalScore",
        })
        .then((response) => {
            setFinalScore(response.data)
        })
        .catch((error) => {
          console.log(error)
        })
      }, [])
      useEffect(() => {
        fetchData()
      }, [fetchData])
    
    return(
        <div className='content'>
        <div className='settings'>
            <span style={{fontSize:"30px"}}>Quiz Settings</span>
            <div className="settings_select">
               <TextField style={{marginBottom:"25px"}} label='Enter Your Name' variant='outlined'
               onChange={(e) => setName(e.target.value)}/>
               {error && <ErrorMessage>Please Enter Your Name</ErrorMessage>}
          
               <button type="button" className="btn btn-primary btn-lg btn-block"
               onClick={handleSubmit}
               style={{cursor:"pointer"}}
               >
                   Start Quiz
               </button>
               
           
            </div>
            <div className='mt-4'>
            {finalscore.name ? <h2>Recently Played Name : {finalscore.name}</h2> : ""}
            {finalscore.final_score ? <h2>Final Score : {finalscore.final_score}</h2> : ""}
            {finalscore.error_count ?  <h2>Number Of Error Counts : {finalscore.error_count}</h2>: ""}
            
            </div>
        </div>
        <img className='banner'
        alt=''
        src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQeq6TEEIzbAg_JZ2CIA_Rz2DtKa8zog2AqaA&usqp=CAU'/>
        </div>
    )
}
export default Home;