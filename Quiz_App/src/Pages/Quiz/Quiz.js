import {useEffect,useState } from "react";
import "./Quiz.css";
import { useHistory } from "react-router";
import { CircularProgress } from "@material-ui/core";
import Question from './../../components/Question/Question';
const Quiz = ({name, questions, score, setScore, setQuestions,errorcount, setErrorCount}) =>{
    const[options,setOptions] = useState(); 
    const history = useHistory();
    const[currQues,setCurrQues] = useState(0); 
        useEffect(() => {
          if(!name){
            history.push("/");
          }
          setOptions(
            questions &&
              handleShuffle([
                questions[currQues]?.correct_option,
                ...questions[currQues]?.incorrect_option,
              ])
          );
        }, [currQues, questions]);
      
        console.log(questions);
      
        const handleShuffle = (options) => {
          return options.sort(() => Math.random() - 0.5);
        };
    return(
        <div className='quiz'>
      <span className="subtitle">Welcome, {name}</span>
   {questions ? <>
        <div className="quizInfo">
        <span></span>
            <span>
              Score : {score}
            </span>
          </div>
            
         <Question
          currQues={currQues}
          setCurrQues={setCurrQues}
          questions={questions}
          options={options}
          correct={questions[currQues]?.correct_option}
          score={score}
          setScore={setScore}
          setQuestions={setQuestions}
          errorcount={errorcount} 
          setErrorCount={setErrorCount}
          name={name}/>
        </>
       : 
        <CircularProgress
        style={{ margin: 100 }}
        color="inherit"
        size={150}
        thickness={1}
        />
      }
        </div>
    )
}
export default Quiz;