import { Button } from "@material-ui/core";
import { useEffect} from "react";
import axios from "axios";
import { useHistory } from "react-router";
import "./Result.css";

const Result = ({ name, setName, score,errorcount,setScore }) => {
  const history = useHistory();

  useEffect(() => {
    if (!name) {
      history.push("/");
    }
  }, [name, history]);

  const handleReStart = async() => {
    const data = {name : name, final_score : score,error_count : errorcount};
    axios.post('https://glacial-ridge-45733.herokuapp.com/scoreRecord', data)
    .then((response) => {
        console.log(response.data)
        setScore(0);
        setName("");
        history.go(0);
    })
    .catch((error) => {
      console.log(error)
    })
    
  };
  return (
    <div className="result">
      <div className="abc">Name : {name}</div>
      <span className="title">Final Score : {score}</span>
      <Button
        variant="contained"
        color="secondary"
        size="large"
        style={{ alignSelf: "center", marginTop: 20 }}
        onClick={handleReStart}>
        Re Start
      </Button>
    </div>
  );
};

export default Result;