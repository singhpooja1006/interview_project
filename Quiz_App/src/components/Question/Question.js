import { Button } from "@material-ui/core";
import { useState } from "react";
import "./Question.css";
import { useHistory } from "react-router";
import ErrorMessage from "../ErrorMessage/ErrorMessage";
const Question = ({
    currQues,
    setCurrQues,
    questions,
    options,
    correct,
    setScore,
    score,
    errorcount, 
    setErrorCount,
    setQuestions,
  })=>{
    const [selected, setSelected] = useState();
    const [error, setError] = useState(false);
    const history = useHistory();

      const handleCheck = (i) => {
        setSelected(i);
        if (i === correct) {
        setError("Option is correct.");
        }
        else if (i !== correct) {
          setError("Please select correct option.");
          setErrorCount(errorcount+1);
        }
        else if (i === correct) {
          setError(false);
        }
      };

      const handleQuit = () => {
        setCurrQues(0);
        setQuestions();
      };
     
      const handleNext = () => {
        if (currQues === (questions.length - 1)) {
          setScore(score + 1);
          history.push("/result");
        } else if (selected === correct) {
          setScore(score + 1);
          setCurrQues(currQues + 1);
          setSelected();
          setError(false);
        } 
        else 
        setError("Please select correct option first");
      };
    return(
        <div className="question">
            <h1>Question {currQues + 1}</h1>
            <div className="singleQuestion">
                  <h2>{questions[currQues].question}</h2>
            <div className="options">
              {error && <ErrorMessage>{error}</ErrorMessage>}
              {options &&
              options.map((i) => (
              <img className="singleOption"
              src={i}
              alt=""
              onClick={() => handleCheck(i)}
              />
              ))}
            </div>
            <div className="controls">
            <Button
              variant="contained"
              color="secondary"
              size="large"
              style={{ width: 185 }}
              href="/"
              onClick={() => handleQuit()}
            >
            Quit
            </Button>
        
            <Button
            variant="contained"
            color="primary"
            size="large"
            style={{ width: 185 }}
            onClick={handleNext}
          >
       {currQues === (questions.length - 1)  ? "Finish" : "Next Question" }
          </Button> 
        </div>
          </div>
        </div>
    )
}
export default Question;